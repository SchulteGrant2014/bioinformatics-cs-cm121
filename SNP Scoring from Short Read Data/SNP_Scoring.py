#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun November 5 21:35:00 2017

@author: Grant Schulte
"""

"""
In this lesson, you will compute the log odds ratio for the presence vs.
absence of a SNP given a set of short read data for a candidate SNP site.
To simplify your task, you are given the basecall likelihoods at this
position for each read, under the assumption that the SNP is present (S')
in that read, vs. absent (S) in that read.  For the SNP model, just assume
the SNP is present in the population with an allele frequency of 20%.  You
should also assume the reads are from a large pooled population sample,
i.e. each read is independent, drawn from a different person.
"""


"""
Compute the log-odds ratio for SNP vs. no-SNP models, as a simple likelihood
odds ratio calculation (i.e. no need to consider priors).  Treat the reads
as independent draws from the population; for the SNP model just assume a
population allele frequency of 20%.  For each read you will receive its
likelihood under the assumption the SNP was absent from that read
(nucleotide S), vs. under the assumption the SNP was present in that read
(nucleotide S'), and its readID.  Return the log odds ratio using natural
log (base e, not base 10).

Sample Input:
0.001,0.99,1

Sample Output:
5.292299294222474
"""

import math
def calc_snp_log_odds(readData): # do not modify this line
    """readData is a list of (p(read|S), p(read|S'), readID) tuples """
    obs_noSNP = 1.0
    obs_SNP = 1.0
    
    for Xi in readData:
        obs_noSNP *= Xi[0]
        obs_SNP *= (Xi[0] * 0.8) + (Xi[1] * 0.2)
    
    logOdds = math.log(obs_SNP) - math.log(obs_noSNP)
    
    return logOdds


"""
Paste your code from the previous step, and extend it to report the log of
the posterior odds ratio of the SNP vs. no-SNP models, assuming that real
SNPs occur on average at one in a thousand positions in the genome.

Sample Input:
0.001,0.99,1

Sample Output:
-1.6144554844260801
"""

def calc_snp_log_odds2(readData, p_snp=0.001): # do not modify this line
    """readData is a list of (p(read|S), p(read|S'), readID) tuples """
    
    pSNP = 1.
    for Xi in readData:
        pSNP *= ((Xi[0] * 0.8) + (Xi[1] * 0.2))
    
    pnoSNP = 1.
    for Xi in readData:
        pnoSNP *= Xi[0]
    
    postNum = pSNP * p_snp
    postDenom = pnoSNP * (1 - p_snp)
    
    odds = postNum / postDenom
    logOdds = math.log(odds)
    return logOdds


"""
Paste in your code from the previous step to test it on the next set of
testcases: big datasets.  Hint: the IEEE floating point standard cannot
represent positive numbers smaller than around 10-200 (i.e. it silently
truncates them to zero), which can lead to both wrong results and math
domain errors. Robust probability calculations on big datasets require
doing the calculations using log-probabilities (i.e. log10_p=-200 instead
of p=1e-200), e.g. log_p=log_p1+log_p2 instead of p=p1*p2.

Sample Input:
0.001,0.99,1

Sample Output:
-1.6144554844260801
"""

def calc_snp_log_odds_BIG_NUMS(readData, p_snp=0.001): # do not modify this line
    """readData is a list of (p(read|S), p(read|S'), readID) tuples """
    
    log_pSNP = 1.
    for Xi in readData:
        log_pSNP += math.log((Xi[0] * 0.8) + (Xi[1] * 0.2))
    
    log_pnoSNP = 1.
    for Xi in readData:
        log_pnoSNP += math.log(Xi[0])
    
    postNum = log_pSNP + math.log(p_snp)
    postDenom = log_pnoSNP + math.log(1 - p_snp)
    
    log_odds = postNum - postDenom
    return log_odds

