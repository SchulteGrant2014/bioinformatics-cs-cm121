#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 18:13:01 2017

@author: grant
"""

"""
You are given a spreadsheet of dice rolls and their probabilities under two models: F, "fair dice" where all rolls have equal probability (1/6); and L, "loaded dice" which rolls a 6 half the time, and any of the other rolls (1, 2, 3, 4, 5) with 10% probability each. The casino switches from F to L with 5% probability on each roll, and from L to F with 10% probability on each roll. 

If you do this challenge in Python, your function will receive

liks: a list of (p(roll|F), p(roll|L)) tuples, for each successive dice roll
ptrans: the transition matrix in the form ((p(F|F), p(L|F)), (p(F|L), p(L|L))
prior: a tuple of the prior probabilities (p(F), p(L))
and your function should return a list of (f_F, f_L) forward probability tuples for each successive dice roll.
In this step, there are only five dice rolls in the sequence.
"""

"""
Sample Input:
params,0.95,0.9,0.6666666666666666,0.3333333333333333
6,0.16666666666666666,0.5
1,0.16666666666666666,0.1
1,0.16666666666666666,0.1
2,0.16666666666666666,0.1
6,0.16666666666666666,0.5
Sample Output:
0.1111111111111111,0.16666666666666666
0.02037037037037037,0.015555555555555555
0.003484567901234567,0.001501851851851852
0.0005767541152263372,0.00015258950617283955
9.386256001371738e-05,8.308413065843624e-05
"""

def calc_forward(liks, ptrans, prior): # don't change this line
    '''
    liks is list of (p(roll|F), p(roll|L)) --> Use liks[roll][0=F;1=L] to access p(roll#1|F);
    ptrans is p[from][to]; 0=F;1=L ... in the form ((p(F|F), p(L|F)), (p(F|L), p(L|L));
    prior is (p(F), p(L))
    '''
    # Instantiate variables to make reading easier
    F = 0
    L = 1
    
    # First forward probability: ex: f_F = p(theta_1 = F) * p(X1 = 6 | theta_1 = F)
    f_F_1 = prior[F]*liks[0][F]
    f_L_1 = prior[L]*liks[0][L]
    forward = [(f_F_1, f_L_1)]
    
    # Calculate the rest of the forward probabilities from the first one
    fp_index = 1; # start filling in the matrix from the second row, which is dependent on the first row
    for rollNum in range(len(liks)-1):
        
        f_F = (forward[fp_index - 1][F] * ptrans[F][F] * liks[fp_index][F]) + (forward[fp_index - 1][L] * ptrans[L][F] * liks[fp_index][F])
        f_L = (forward[fp_index - 1][F] * ptrans[F][L] * liks[fp_index][L]) + (forward[fp_index - 1][L] * ptrans[L][L] * liks[fp_index][L])
        forward.append( (f_F,f_L) )
        fp_index += 1
    
    return forward


"""
In this step, we simply extend our calculation from the previous step to include:

the f_F, f_L forward probability values for all rows;
the b_F, b_L backward probability values for all rows
In this step, there are only five dice rolls in the sequence.
"""

"""
Sample Input:
params,0.95,0.9,0.6666666666666666,0.3333333333333333
2,0.16666666666666666,0.1
6,0.16666666666666666,0.5
2,0.16666666666666666,0.1
1,0.16666666666666666,0.1
6,0.16666666666666666,0.5
Sample Output:
0.1111111111111111,0.03333333333333333,0.0009363167438271605,0.002146471450617284
0.018148148148148146,0.01777777777777778,0.005190787037037037,0.0045776851851851855
0.0031697530864197524,0.0016907407407407414,0.03136111111111111,0.04505555555555556
0.0005300565843621397,0.00016801543209876552,0.18333333333333335,0.4666666666666667
8.672588305898487e-05,8.8858359053498e-05,1.0,1.0
"""

def calc_fb(liks, ptrans, prior): # don't change this line
    '''
    liks is list of (p(roll|F), p(roll|L)) --> Use liks[roll][0=F;1=L] to access p(roll#1|F);
    ptrans is p[from][to]; 0=F;1=L ... in the form ((p(F|F), p(L|F)), (p(F|L), p(L|L));
    prior is (p(F), p(L))
    '''
    # Instantiate variables to make reading easier
    F = 0
    L = 1
    
    # First forward probability: ex: f_F = p(theta_1 = F) * p(X1 = 6 | theta_1 = F)
    f_F_1 = prior[F]*liks[0][F]
    f_L_1 = prior[L]*liks[0][L]
    forward = [(f_F_1, f_L_1)]
    
    # First backward probability: Bottom rows = 1
    backward = [(1.0,1.0) for row in range(len(liks))]
    # Next-to-last backward probability: ex: b_F = p(roll_t|theta_t)*p(theta_t|theta_t-1)
    b_F_ntl = (liks[len(liks) - 1][F] * ptrans[F][F]) + (liks[len(liks) - 1][L] * ptrans[F][L])
    b_L_ntl = (liks[len(liks) - 1][F] * ptrans[L][F]) + (liks[len(liks) - 1][L] * ptrans[L][L])
    backward[len(liks) - 2] = (b_F_ntl, b_L_ntl)
    
    # Calculate the rest of the forward probabilities from the first one
    fp_index = 1; # start filling in the matrix from the second row, which is dependent on the first row
    for rollNum in range(len(liks)-1):
        # Forward Probabilities
        f_F = (forward[fp_index - 1][F] * ptrans[F][F] * liks[fp_index][F]) + (forward[fp_index - 1][L] * ptrans[L][F] * liks[fp_index][F])
        f_L = (forward[fp_index - 1][F] * ptrans[F][L] * liks[fp_index][L]) + (forward[fp_index - 1][L] * ptrans[L][L] * liks[fp_index][L])
        forward.append( (f_F,f_L) )
        fp_index += 1
    
    # Calculate the rest of the backward probabilities from the one previously calculated
    bp_index = len(liks) - 3
    for rollNum in range(len(liks) - 2):
        # Backward Probabilities
        b_F = ptrans[F][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[F][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        b_L = ptrans[L][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[L][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        backward[bp_index] = (b_F, b_L)
        bp_index -= 1
    
    
    return forward, backward


"""
In this step, we simply extend our calculation from the previous step to include:

the f_F, f_L forward probability values for all rows;
the b_F, b_L backward probability values for all rows;
p(obs) computed for all rows
In this step, there are only five dice rolls in the sequence.
"""

"""
Sample Input:
params,0.95,0.9,0.6666666666666666,0.3333333333333333
2,0.16666666666666666,0.1
4,0.16666666666666666,0.1
1,0.16666666666666666,0.1
4,0.16666666666666666,0.1
5,0.16666666666666666,0.1
Sample Output:
0.1111111111111111,0.03333333333333333,0.0006791936882716048,0.000210080524691358,8.246864951989023e-05
0.018148148148148146,0.003555555555555556,0.004240731481481481,0.0015489074074074073,8.246864951989025e-05
0.0029327160493827155,0.0004107407407407409,0.026394444444444443,0.012322222222222222,8.246864951989023e-05
0.00047119238683127556,5.163024691358028e-05,0.16333333333333333,0.10666666666666667,8.246864951989023e-05
7.546596536351163e-05,7.0026841563786055e-06,1.0,1.0,8.246864951989023e-05
"""

def calc_fb(liks, ptrans, prior): # don't change this line
    '''
    liks is list of (p(roll|F), p(roll|L)) --> Use liks[roll][0=F;1=L] to access p(roll#1|F);
    ptrans is p[from][to]; 0=F;1=L ... in the form ((p(F|F), p(L|F)), (p(F|L), p(L|L));
    prior is (p(F), p(L))
    '''
    # Instantiate variables to make reading easier
    F = 0
    L = 1
    
    # First forward probability: ex: f_F = p(theta_1 = F) * p(X1 = 6 | theta_1 = F)
    f_F_1 = prior[F]*liks[0][F]
    f_L_1 = prior[L]*liks[0][L]
    forward = [(f_F_1, f_L_1)]
    
    # First backward probability: Bottom rows = 1
    backward = [(1.0,1.0) for row in range(len(liks))]
    # Next-to-last backward probability: ex: b_F = p(roll_t|theta_t)*p(theta_t|theta_t-1)
    b_F_ntl = (liks[len(liks) - 1][F] * ptrans[F][F]) + (liks[len(liks) - 1][L] * ptrans[F][L])
    b_L_ntl = (liks[len(liks) - 1][F] * ptrans[L][F]) + (liks[len(liks) - 1][L] * ptrans[L][L])
    backward[len(liks) - 2] = (b_F_ntl, b_L_ntl)
    
    # Calculate the rest of the forward probabilities from the first one
    fp_index = 1; # start filling in the matrix from the second row, which is dependent on the first row
    for rollNum in range(len(liks)-1):
        # Forward Probabilities
        f_F = (forward[fp_index - 1][F] * ptrans[F][F] * liks[fp_index][F]) + (forward[fp_index - 1][L] * ptrans[L][F] * liks[fp_index][F])
        f_L = (forward[fp_index - 1][F] * ptrans[F][L] * liks[fp_index][L]) + (forward[fp_index - 1][L] * ptrans[L][L] * liks[fp_index][L])
        forward.append( (f_F,f_L) )
        fp_index += 1
    
    # Calculate the rest of the backward probabilities from the one previously calculated
    bp_index = len(liks) - 3
    for rollNum in range(len(liks) - 2):
        # Backward Probabilities
        b_F = ptrans[F][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[F][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        b_L = ptrans[L][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[L][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        backward[bp_index] = (b_F, b_L)
        bp_index -= 1
    
    # Calculate p(obs) for each row
    pobs = []
    for i in range(len(liks) - 1): # Leave room for the summation at the end
        pobs.append( (forward[i][F] * backward[i][F]) + (forward[i][L] * backward[i][L]) )
    
    pobs.append( forward[len(liks) - 1][F] + forward[len(liks) - 1][L] )
    
    return forward, backward, pobs


"""
In this step, we simply extend our calculation from the previous step to include:
forward: a list of (f_F, f_L) forward probability tuples for each successive dice roll;
backward: a list of (b_F, b_L) backward probability tuples for each successive dice roll;
pobs: a list of p(obs) values (probability of all the rolls) computed from the forward-backward values for each roll (this is a useful check; they should all be the same!);
ppost: a list of (p(F|obs), p(L|obs)) posterior probability tuples for the F vs. L hidden state at each successive dice roll.
In this step, there are only five dice rolls in the sequence.
"""

"""
Sample Input:
params,0.95,0.9,0.6666666666666666,0.3333333333333333
5,0.16666666666666666,0.1
5,0.16666666666666666,0.1
3,0.16666666666666666,0.1
2,0.16666666666666666,0.1
6,0.16666666666666666,0.5
Sample Output:
0.1111111111111111,0.03333333333333333,0.0008447630401234568,0.0004985047839506174,0.00011047938614540466,0.8495934245161588,0.15040657548384123
0.018148148148148146,0.003555555555555556,0.005190787037037037,0.0045776851851851855,0.00011047938614540466,0.8526764624637937,0.14732353753620628
0.0029327160493827155,0.0004107407407407409,0.03136111111111111,0.04505555555555556,0.00011047938614540466,0.8324922602392261,0.1675077397607739
0.00047119238683127556,5.163024691358028e-05,0.18333333333333335,0.4666666666666667,0.00011047938614540466,0.781913024076486,0.21808697592351392
7.546596536351163e-05,3.5013420781893024e-05,1.0,1.0,0.00011047938614540466,0.6830773413620257,0.31692265863797425
"""


def calc_fb(liks, ptrans, prior): # don't change this line
    '''
    liks is list of (p(roll|F), p(roll|L)) --> Use liks[roll][0=F;1=L] to access p(roll#1|F);
    ptrans is p[from][to]; 0=F;1=L ... in the form ((p(F|F), p(L|F)), (p(F|L), p(L|L));
    prior is (p(F), p(L))
    '''
    # Instantiate variables to make reading easier
    F = 0
    L = 1
    
    # First forward probability: ex: f_F = p(theta_1 = F) * p(X1 = 6 | theta_1 = F)
    f_F_1 = prior[F]*liks[0][F]
    f_L_1 = prior[L]*liks[0][L]
    forward = [(f_F_1, f_L_1)]
    
    # First backward probability: Bottom rows = 1
    backward = [(1.0,1.0) for row in range(len(liks))]
    # Next-to-last backward probability: ex: b_F = p(roll_t|theta_t)*p(theta_t|theta_t-1)
    b_F_ntl = (liks[len(liks) - 1][F] * ptrans[F][F]) + (liks[len(liks) - 1][L] * ptrans[F][L])
    b_L_ntl = (liks[len(liks) - 1][F] * ptrans[L][F]) + (liks[len(liks) - 1][L] * ptrans[L][L])
    backward[len(liks) - 2] = (b_F_ntl, b_L_ntl)
    
    # Calculate the rest of the forward probabilities from the first one
    fp_index = 1; # start filling in the matrix from the second row, which is dependent on the first row
    for rollNum in range(len(liks)-1):
        # Forward Probabilities
        f_F = (forward[fp_index - 1][F] * ptrans[F][F] * liks[fp_index][F]) + (forward[fp_index - 1][L] * ptrans[L][F] * liks[fp_index][F])
        f_L = (forward[fp_index - 1][F] * ptrans[F][L] * liks[fp_index][L]) + (forward[fp_index - 1][L] * ptrans[L][L] * liks[fp_index][L])
        forward.append( (f_F,f_L) )
        fp_index += 1
    
    # Calculate the rest of the backward probabilities from the one previously calculated
    bp_index = len(liks) - 3
    for rollNum in range(len(liks) - 2):
        # Backward Probabilities
        b_F = ptrans[F][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[F][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        b_L = ptrans[L][F] * liks[bp_index + 1][F] * backward[bp_index + 1][F] + ptrans[L][L] * liks[bp_index + 1][L] * backward[bp_index + 1][L]
        backward[bp_index] = (b_F, b_L)
        bp_index -= 1
    
    # Calculate p(obs) for each row
    pobs = []
    for i in range(len(liks) - 1): # Leave room for the summation at the end
        pobs.append( (forward[i][F] * backward[i][F]) + (forward[i][L] * backward[i][L]) )
    
    pobs.append( forward[len(liks) - 1][F] + forward[len(liks) - 1][L] )
    
    # Calculate the posterior probabilities p(thetat=F|X) and p(thetat=L|X) for every row
    ppost = []
    for i in range(len(liks)):
        posterior_F = (forward[i][F] * backward[i][F]) / pobs[i]
        posterior_L = (forward[i][L] * backward[i][L]) / pobs[i]
        ppost.append( (posterior_F, posterior_L) )
    
    return forward, backward, pobs, ppost


