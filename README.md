# README #

These were some of the programming projects for the Bioinformatics class CS CM121 at UCLA.
The code is short for each because it is the result of condensing many pages of statistical calculations into a few Python functions.
The projects are organized by folder:

1. SNP Scoring from Short Read Data (Determining odds of a SNP existing in the population)
2. Phenotype Sequencing (Hypothesis Testing)
3. Dynamic Programming Sequence Alignment (Align DNA/RNA/Protein sequences using scoring functions + Viterbi algorithm)
4. Casino Royal HMM (Hidden Markov Model used to detect cheating at a casino given a string of dice roll observations)