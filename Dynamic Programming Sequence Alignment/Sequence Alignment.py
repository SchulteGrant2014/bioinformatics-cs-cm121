#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 23:04:40 2017

@author: grant
"""


"""
In this step you will calculate the Viterbi optimal score matrix for a
small example (very short sequences).  You are given four input arguments:

X, Y: the two sequences to align
S: the letter substitution score matrix, i.e. S['A']['G'] = score for A vs G
g: the gap penalty

Your code must return a single matrix scores, such that scores[t][u] is the
optimal Viterbi score for the best possible alignment of the first t letters
of sequence X vs. the first u letters of sequence Y.

If you code this in Python, your function will simply receive as the arguments
X, Y, S and g directly. Your Python function should simply return your
calculated scores matrix (e.g. as a Python list of lists). If you use a
language other than Python, you will have to read the input parameters (two
sequences, gap penalty, and substitution scores table) in CSV format, and
write your calculated scores matrix out in CSV format.

Sample Input:
GGTGGT,GCTGCT
-3,G,T,A,C
G,5,-2,-2,-2
T,-2,5,-2,-2
A,-2,-2,5,-2
C,-2,-2,-2,5

Sample Output:
0,-3,-6,-9,-12,-15,-18
-3,5,2,-1,-4,-7,-10
-6,2,3,0,-3,-6,-9
-9,-1,0,8,5,2,-1
-12,-4,4,5,13,10,7
-15,-7,1,2,10,11,8
-18,-10,-2,6,7,8,16
"""

# S = {'T': {'T': 5, 'C': -2, 'A': -2, 'G': -2}, 'C': {'T': -2, 'C': 5, 'A': -2, 'G': -2}, 'A': {'T': -2, 'C': -2, 'A': 5, 'G': -2}, 'G': {'T': -2, 'C': -2, 'A': -2, 'G': 5}}

def global_viterbi_scores(X, Y, S, g): # do not change this line
    """X, Y are two sequences to align, S is substitution score[x][y] and g is the gap penalty"""
    
    scores = [[0 for y in range(len(Y)+1)] for x in range(len(X)+1)] # scores[t][u] = optimal score for aligning first t letters of X, u letters of Y
    # each tuple in "scores" is a column
    
    # Fill in the edges of the scores matrix, i.e.: 0, -3, -6, -9...
    for t in range(len(X)+1):
        scores[t][0] = g*t  # scores[column][row]
    for u in range(len(Y)+1):
        scores[0][u] = g*u
    
    # Fill out the rest of the table based on surrounding scores using the Viterbi Algorithm
    for t in range(1, len(X)+1):
        for u in range(1, len(Y)+1):  # Start at cell (1,1) and go to end of sequences
            m_move = scores[t-1][u-1] + S[X[t-1]][Y[u-1]]
            x_move = scores[t-1][u] + g
            y_move = scores[t][u-1] + g
            scores[t][u] = max(m_move, x_move, y_move)
    
    return scores


print("VITERBI #1 - ONLY SCORES:")
result1 = global_viterbi_scores("GGTGGT", "GCTGCT", {'T': {'T': 5, 'C': -2, 'A': -2, 'G': -2}, 'C': {'T': -2, 'C': 5, 'A': -2, 'G': -2}, 'A': {'T': -2, 'C': -2, 'A': 5, 'G': -2}, 'G': {'T': -2, 'C': -2, 'A': -2, 'G': 5}}, -3)
for row in range(len(result1)):
    for column in result1:
        print(column[row], end=" ")
    print()
print()


"""
Same as before, BUT...

Your code must return two matrices:
    
    1. A matrix "scores", such that scores[t][u] is the optimal Viterbi score
    for the best possible alignment of the first t letters of sequence X vs. the
    first u letters of sequence Y.

    2. A matrix "moves", such that moves[t][u] is the optimal Viterbi move
    arriving at alignment matrix cell (t,u).

If you code this in Python, your function will simply receive as the arguments
X, Y, S and g directly. Your Python function should simply return your
calculated scores matrix (e.g. as a Python list of lists) and your moves
matrix. If you use a language other than Python, you will have to read the
input parameters (two sequences, gap penalty, and substitution scores table)
in CSV format, and write your calculated scores and moves matrices out in CSV
format.

Sample Input:
CGTGCG,CAGTGG
-3,T,C,A,G
T,5,-2,-2,-2
C,-2,5,-2,-2
A,-2,-2,5,-2
G,-2,-2,-2,5

Sample Output:
0,-3,-6,-9,-12,-15,-18
-3,5,2,-1,-4,-7,-10
-6,2,3,0,-3,-6,-9
-9,-1,7,4,5,2,-1
-12,-4,4,12,9,6,3
-15,-7,1,9,17,14,11
-18,-10,-2,6,14,15,19
0,x,x,x,x,x,x
y,m,x,x,x,x,x
y,y,m,x,x,x,x
y,y,m,x,m,x,x
y,y,y,m,x,x,x
y,y,y,y,m,x,x
y,y,y,y,y,m,m
"""

# S = {'T': {'T': 5, 'C': -2, 'A': -2, 'G': -2}, 'C': {'T': -2, 'C': 5, 'A': -2, 'G': -2}, 'A': {'T': -2, 'C': -2, 'A': 5, 'G': -2}, 'G': {'T': -2, 'C': -2, 'A': -2, 'G': 5}}

def global_viterbi_scoresAndMoves(X, Y, S, g): # do not change this line
    """X, Y are two sequences to align, S[L1][L2] is the substitution score, and g is the gap penalty"""
    # replace this with your actual calculation
    scores = [[0 for y in range(len(Y)+1)] for x in range(len(X)+1)] # scores[t][u] = optimal score for aligning first t letters of X, u letters of Y. Each element (list) in scores is a column.
    moves = [[' ' for y in range(len(Y)+1)] for x in range(len(X)+1)] # moves[t][u] = optimal move for arriving at cell (t,u)
    
    # Fill in the edges of the scores and moves matricees, i.e.: (0,x), (-3,x), (-6,x), (-9,x)...
    for t in range(len(X)+1):
        scores[t][0] = g*t  # scores[column][row]
        moves[t][0] = 'x'  # moves[column][row]
    for u in range(len(Y)+1):
        scores[0][u] = g*u
        moves[0][u] = 'y'
    moves[0][0] = '0'
    
    # Fill out the rest of the table based on surrounding scores using the Viterbi Algorithm
    for t in range(1, len(X)+1):
        for u in range(1, len(Y)+1):  # Start at cell (1,1) and go to end of sequences
            m_move = scores[t-1][u-1] + S[X[t-1]][Y[u-1]]
            x_move = scores[t-1][u] + g
            y_move = scores[t][u-1] + g
            
            maximumScore = max(m_move, x_move, y_move)
            scores[t][u] = maximumScore
            if maximumScore == x_move:
                moves[t][u] = 'x'
            elif maximumScore == y_move:
                moves[t][u] = 'y'
            else:
                moves[t][u] = 'm'
    
    return scores, moves


print("VITERBI #2 - SCORES AND MOVES:")
result2 = global_viterbi_scoresAndMoves("CGTGCG", "CAGTGG", {'T': {'T': 5, 'C': -2, 'A': -2, 'G': -2}, 'C': {'T': -2, 'C': 5, 'A': -2, 'G': -2}, 'A': {'T': -2, 'C': -2, 'A': 5, 'G': -2}, 'G': {'T': -2, 'C': -2, 'A': -2, 'G': 5}}, -3)

for row in range(len(result2[0])):
    for column in result2[0]:
        print(column[row], end=" ")
    print()
for row in range(len(result2[1])):
    for column in result2[1]:
        print(column[row], end=" ")
    print()
print()



"""
Same as before, but now...

Your code must return a list of aligned positions (t,u) for the best possible
global alignment of sequence X vs. sequence Y.

Note that:
    1. You should express t and u in standard Python/C zero-based indexing,
        i.e. t=0 is the first letter of sequence X, t=1 is the second letter
        of sequence X, etc.
    2. Your list should be in ascending order, i.e. lowest values of t first.

If you code this in Python, your function will simply receive as the arguments
X, Y, S and g directly. Your Python function should simply return your
alignment (e.g. as a Python list of tuples). If you use a language other than
Python, you will have to read the input parameters (two sequences, gap
penalty, and substitution scores table) in CSV format, and write your
calculated alignment list out in CSV format.

Sample Input:
TCCGAA,CCGGTA
-3,A,G,C,T
A,5,-2,-2,-2
G,-2,5,-2,-2
C,-2,-2,5,-2
T,-2,-2,-2,5

Sample Output:
1,0
2,1
3,2
4,3
5,5
"""

def align(X, Y, S, g): # do not change this line
    """X, Y are two sequences to align, S is scores[x][y] and g is the gap penalty"""
    scores = [[0 for y in range(len(Y)+1)] for x in range(len(X)+1)] # scores[t][u] = optimal score for aligning first t letters of X, u letters of Y. Each element (list) in scores is a column.
    moves = [[' ' for y in range(len(Y)+1)] for x in range(len(X)+1)] # moves[t][u] = optimal move for arriving at cell (t,u)
    path = [] # list of diagonal moves that define the optimal path
    
    # Fill in the edges of the scores and moves matricees, i.e.: (0,x), (-3,x), (-6,x), (-9,x)...
    for t in range(len(X)+1):
        scores[t][0] = g*t  # scores[column][row]
        moves[t][0] = 'x'  # moves[column][row]
    for u in range(len(Y)+1):
        scores[0][u] = g*u
        moves[0][u] = 'y'
    moves[0][0] = '0'
    
    # Fill out the rest of the table based on surrounding scores using the Viterbi Algorithm
    for t in range(1, len(X)+1):
        for u in range(1, len(Y)+1):  # Start at cell (1,1) and go to end of sequences
            m_move = scores[t-1][u-1] + S[X[t-1]][Y[u-1]]
            x_move = scores[t-1][u] + g
            y_move = scores[t][u-1] + g
            
            maximumScore = max(m_move, x_move, y_move)
            scores[t][u] = maximumScore
            if maximumScore == y_move:
                moves[t][u] = 'y'
            elif maximumScore == x_move:
                moves[t][u] = 'x'
            else:
                moves[t][u] = 'm'
    
    # Backtrack through the matrix, picking out the cells along the optimal path where a diagonal was taken to GET TO that cell
    x_index = len(X)  # Bottom-right index of the matrix, "moves" has an extra row and column
    y_index = len(Y)  # Bottom-right index of the matrix, "moves" has an extra row and column
    atBeginning = False
    while ( not atBeginning ):
        
        if (moves[x_index][y_index] == 'm'):
            path.append( (x_index - 1, y_index - 1) )
            x_index -= 1
            y_index -= 1
        elif (moves[x_index][y_index] == 'x'):
            x_index -= 1
        else:
            y_index -= 1
        
        if (x_index == 0 and y_index == 0):
            atBeginning = True
    
    return reversed(path)


print("VITERBI #3 - OPTIMAL PATH FINDING:")
result3 = align("TCCGAA", "CCGGTA", {'T': {'T': 5, 'C': -2, 'A': -2, 'G': -2}, 'C': {'T': -2, 'C': 5, 'A': -2, 'G': -2}, 'A': {'T': -2, 'C': -2, 'A': 5, 'G': -2}, 'G': {'T': -2, 'C': -2, 'A': -2, 'G': 5}}, -3)

for path in result3:
    print(path)
print()





